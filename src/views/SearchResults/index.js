import React, { Component } from 'react';

import QuarterGrid from '../../components/QuarterGrid/quarterGrid';
import PageContainer from '../../components/PageContainer/pageContainer';
import NotFoundSearch from '../../components/NotFoundSearch/notFoundSearch';

import { getResultsSearch } from '../../services/resultsSearch';
import { parseSearch } from '../../utils/spotifyResponseParsers';
import { serverError } from '../../utils/errors';

import './searchResults.css';
import TracksGrid from '../../components/Grid/tracksGrid';
import ArtistsGrid from '../../components/Grid/artistsGrid';
import AlbumsGrid from '../../components/Grid/albumsGrid';
import PlaylistGrid from '../../components/Grid/playlistGrid';

const titleSongs = 'Songs';
const titleArtists = 'Artists';
const titleAlbums = 'Albums';
const titlePlaylists = 'Playlists';
const subtitle = 'RESULTS';

export default class searchResults extends Component {
  state = {
    artists: [],
    albums: [],
    tracks: [],
    playlists: [],
    lastQuery: '',
    loaded: false
  };

  searchQuery = () => {
    const { lastQuery } = this.state;
    const {
      match: {
        params: { query }
      }
    } = this.props;
    if (lastQuery !== query) {
      getResultsSearch(query)
        .then((response) => {
          const searchData = parseSearch(response);
          this.setState({
            ...searchData,
            loaded: true
          });
        })
        .catch((error) => {
          this.setState({ error: serverError(error) });
        });
      this.setState({ lastQuery: query });
    }
  };

  componentDidMount = () => {
    this.searchQuery();
  };

  componentDidUpdate = () => {
    this.searchQuery();
  };

  render = () => {
    const {
      playlists,
      tracks,
      albums,
      artists,
      lastQuery,
      loaded,
      error
    } = this.state;
    const totalLength =
      tracks.length + artists.length + albums.length + playlists.length;

    return (
      <PageContainer {...{ error, loaded }}>
        <div className="search-results">
          <div className="search-results__title">
            Search results for:
            <div className="search-results__result">{lastQuery}</div>
          </div>
          {totalLength > 0 ? (
            <div className="search-results__wrap">
              <QuarterGrid
                title={titleSongs}
                subtitle={`${tracks.length} ${subtitle}`}
                length={tracks.length}
              >
                {!!tracks.length ? (
                  <TracksGrid tracks={tracks.slice(0, 4)} size="quarter" />
                ) : (
                  <NotFoundSearch item="songs" />
                )}
              </QuarterGrid>
              <div className="search-results__divider-vertical" />
              <QuarterGrid
                title={titleArtists}
                subtitle={`${artists.length} ${subtitle}`}
                length={artists.length}
              >
                {!!artists.length ? (
                  <ArtistsGrid artists={artists.slice(0, 4)} size="quarter" />
                ) : (
                  <NotFoundSearch item="artists" />
                )}
              </QuarterGrid>
              <div className="search-results__divider-horizontal" />
              <QuarterGrid
                title={titleAlbums}
                subtitle={`${albums.length} ${subtitle}`}
                length={albums.length}
              >
                {!!albums.length ? (
                  <AlbumsGrid albums={albums.slice(0, 4)} size="quarter" />
                ) : (
                  <NotFoundSearch item="albums" />
                )}
              </QuarterGrid>
              <div className="search-results__divider-vertical" />
              <QuarterGrid
                title={titlePlaylists}
                subtitle={`${playlists.length} ${subtitle}`}
                length={playlists.length}
              >
                {!!playlists.length ? (
                  <PlaylistGrid
                    playlists={playlists.slice(0, 4)}
                    size="quarter"
                  />
                ) : (
                  <NotFoundSearch item="playlist" />
                )}
              </QuarterGrid>
            </div>
          ) : (
            <NotFoundSearch item="search" />
          )}
        </div>
      </PageContainer>
    );
  };
}
